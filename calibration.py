import numpy as np
import logging
import geopy.distance
import os
import pickle
from pathlib import Path

from SOAI.handler.SOAIDiskHandler import SOAIDiskHandler
from SOAI.tools.SOAICalibrator import SOAICalibrator


# load data and use calibration class for each sensor
def main():

    # Clear log
    if os.path.exists(Path(os.environ.get("SOAI") + "/savedModels/result.txt")):
        os.remove(Path(os.environ.get("SOAI") + "/savedModels/result.txt"))

    # Get data
    logger = logging.getLogger()

    dataHandler = SOAIDiskHandler()

    dfOpenAir = dataHandler.fGetOpenAir("./data/openair/", selectValidData=True)
    dfOpenAirLocation = dataHandler.fGetOpenAirSensors()

    dfLanuv = dataHandler.fGetLanuv("./data/lanuv/")
    dfLanuvLocation = dataHandler.fGetLanuvSensors()

    logger.debug("Lanuv and OpenAirData is available")

    # Remove unecessary information and set time as an index
    dfOpenAir = dfOpenAir.drop(["pm10", "pm25", "rssi"], axis=1)
    dfLanuv = dfLanuv.drop(["NO", "OZON"], axis=1)

    dfOpenAir = dfOpenAir.set_index("timestamp", drop=True)
    dfLanuv = dfLanuv.set_index("timestamp", drop=True)

    # Get a list of tuples (lat, lon) for each sensor
    openAirLocations = list(zip(dfOpenAirLocation["lat"], dfOpenAirLocation["lon"]))
    lanuvLocations = list(zip(dfLanuvLocation["lat"], dfLanuvLocation["lon"]))

    """Find the closest lanuv station to EACH openair sensor using a distance matrix and then calibrate"""
    # Loop through all openair locations
    for counter, openSensor in enumerate(openAirLocations):

        # Find closest lanuv sensor to openair sensor
        distanceMatrix = np.zeros((len(lanuvLocations)))

        for countLanuv, lanuvSensor in enumerate(lanuvLocations, 0):  # For each sensor from Lanuv
            distanceMatrix[countLanuv] = geopy.distance.vincenty(openSensor, lanuvSensor).m  # Calcualte the distance

        lanuvSensor = np.where(distanceMatrix == distanceMatrix.min())

        # Select only data of these two pairs
        OpenAirSensorID = dfOpenAirLocation.iloc[counter]["feed"]
        LanuvSensorID = dfLanuvLocation.iloc[lanuvSensor[0][0]]["station"]

        singleDfOpenAir = dfOpenAir[dfOpenAir["feed"] == OpenAirSensorID]
        singleDfLanuv = dfLanuv[dfLanuv["station"] == LanuvSensorID]

        # Create model, if openair & lanuv values are not empty
        if singleDfOpenAir.empty is False and singleDfLanuv.empty is False:
            calibration = SOAICalibrator(singleDfOpenAir, singleDfLanuv, OpenAirSensorID)
            calibration.calibrate()
            calibration.plotPreprocessedInput(Path(os.environ.get("SOAI") + "/savedModels/plots"), distanceMatrix.min(), LanuvSensorID)
            calibration.plotModel(Path(os.environ.get("SOAI") + "/savedModels/plots"))

            # Save results
            calibration.model.save(os.environ.get("SOAI") + f"/savedModels/{OpenAirSensorID}.h5")
            pickle.dump(calibration.scaler, open(Path(os.environ.get("SOAI") + f"/savedModels/{OpenAirSensorID}_scaler.sav"), 'wb'))
            with open(Path(os.environ.get("SOAI") + "/savedModels/result.txt"), "a") as f:
                f.write(f"model saved - MSE: {calibration.evaluatedModel}; {OpenAirSensorID}, {LanuvSensorID}\n{singleDfOpenAir.describe()}\n\n\n\n")

        # Save error, if dataframe of sensor es empty
        else:
            with open(Path(os.environ.get("SOAI") + "/savedModels/result.txt"), "a") as f:
                f.write(f"no model created - empty DataFrame: {OpenAirSensorID}\n\n\n\n")


if __name__ == '__main__':
    main()
