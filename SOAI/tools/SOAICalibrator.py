import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from tensorflow import keras


## Calibrate sensors
class SOAICalibrator:

    ## initiallize variables
    def __init__(self, sensorToCalibrate, sensorCounterpart, OpenAirSensorID):
        self.dfOpenAir = sensorToCalibrate
        self.dfLanuv = sensorCounterpart
        self.OpenAirSensorID = OpenAirSensorID

    ## preprocess data and create model
    def calibrate(self):

        """Merge the OpenAir Cologn and Lanuv data in one data frame"""
        # Downsample the data (in the picture above the non-exciting values are filled with NaN)
        self.dfOpenAir = self.dfOpenAir.resample("1h").mean()
        self.dfLanuv = self.dfLanuv.resample("1h").mean()

        # Make sure that both data sets start and finish at the same time vales (are of the same length)
        timeMin = min(self.dfOpenAir.index.min(), self.dfLanuv.index.min())
        timeMax = max(self.dfOpenAir.index.max(), self.dfOpenAir.index.max())

        self.dfLanuvTime = self.dfLanuv.loc[(self.dfLanuv.index >= timeMin) & (self.dfLanuv.index <= timeMax)]
        self.dfOpenAirTime = self.dfOpenAir.loc[(self.dfOpenAir.index >= timeMin) & (self.dfOpenAir.index <= timeMax)]

        # Combine datasets
        self.dfCombined = pd.DataFrame(index=self.dfOpenAirTime.index)
        self.dfCombined["hum"] = self.dfOpenAirTime["hum"]
        self.dfCombined["temp"] = self.dfOpenAirTime["temp"]
        self.dfCombined["r2"] = self.dfOpenAirTime["r2"]
        self.dfCombined["no2"] = self.dfLanuvTime["no2"]
        self.dfCombined = self.dfCombined.dropna()

        """Train calibration model (NN)"""
        # Select features and scale them
        fHum = self.dfCombined["hum"].values
        fTemp = self.dfCombined["temp"].values
        fR2 = self.dfCombined["r2"].values

        features = np.vstack([fHum, fTemp, fR2]).T
        target = self.dfCombined["no2"].values

        self.scaler = MinMaxScaler()
        featuresScaled = self.scaler.fit_transform(features)

        # Split in train and test set
        trainFeatures, testFeatures, trainTarget, self.testTarget = train_test_split(featuresScaled, target, test_size=0.2)

        # Define NN via keras functional API
        input1 = keras.layers.Input(3)
        dense1 = keras.layers.Dense(10, activation="relu")(input1)
        output1 = keras.layers.Dense(1)(dense1)

        self.model = keras.Model(inputs=[input1], outputs=[output1])
        self.model.compile(loss="mse", optimizer=keras.optimizers.Adam(0.005))

        # Train the model
        self.historyCallback = self.model.fit(trainFeatures, trainTarget, epochs=100, batch_size=10, validation_split=0.1)

        # 5. Evaluate on test set
        self.evaluatedModel = self.model.evaluate(testFeatures, self.testTarget)
        self.predictionTarget = self.model.predict(testFeatures)

    ## plot the preprocessed input
    def plotPreprocessedInput(self, savePath, geoDistance, LanuvSensorID):

        # Plot the data for a intermediate check
        plt.subplot(3, 2, 1)
        self.dfLanuvTime["no2"].plot(title="Lanuv")

        plt.subplot(3, 2, 3)
        self.dfOpenAirTime["r2"].plot(figsize=(20, 10), linewidth=1, fontsize=20, title="OpenAir")  #(lambda x: x/10-90)

        plt.subplot(3, 2, 5)
        dfLanuvNorm = (self.dfLanuvTime["no2"] - self.dfLanuvTime["no2"].mean()) / (self.dfLanuvTime["no2"].max() - self.dfLanuvTime["no2"].min())
        dfOpenAirNorm = (self.dfOpenAirTime["r2"] - self.dfOpenAirTime["r2"].mean()) / (self.dfOpenAirTime["r2"].max() - self.dfOpenAirTime["r2"].min())
        dfLanuvNorm.plot()
        dfOpenAirNorm.plot()

        plt.subplot(3, 2, 6)
        stringBuffer = self.dfCombined.describe()
        plotText = f"OpenAir: {self.OpenAirSensorID}\nLanuv:{LanuvSensorID}\nAccuracy:{self.evaluatedModel}\ngeoDistance: {geoDistance}\ninfo:\n{stringBuffer}"
        plt.text(0.05, 0.095, plotText, fontsize=20)

        plt.savefig(f"{savePath}/1_plot_{self.OpenAirSensorID}.png")

        # Plot data without time dimension
        plt.figure(figsize=(20, 10))

        plt.subplot(3, 1, 1)
        plt.title("Lanuv")
        plt.plot(self.dfCombined["no2"].values)

        plt.subplot(3, 1, 2)
        plt.title("OpenAir")
        plt.plot(self.dfCombined["r2"].values)  # apply(lambda x: x/10-90).values) # Again a very rough approximation to scale and shift by eye

        plt.subplot(3, 1, 3)
        dfLanuvNormCombined = (self.dfCombined["no2"].values - self.dfCombined["no2"].values.mean()) / (self.dfCombined["no2"].values.max() - self.dfCombined["no2"].values.min())
        dfOpenAirNormCombined = (self.dfCombined["r2"].values - self.dfCombined["r2"].values.mean()) / (self.dfCombined["r2"].values.max() - self.dfCombined["r2"].values.min())
        plt.plot(dfLanuvNormCombined)
        plt.plot(dfOpenAirNormCombined)

        plt.savefig(f"{savePath}/2_dataWithoutDimensions_{self.OpenAirSensorID}.png")

        plt.figure(figsize=(20, 10))
        plt.scatter(self.dfCombined["r2"].values, self.dfCombined["no2"].values)
        plt.savefig(f"{savePath}/3_scatterDataWithoutDimensions{self.OpenAirSensorID}.png")

    # plot the model
    def plotModel(self, savePath):

        plt.figure(figsize=(20, 10))
        plt.plot(self.historyCallback.history["loss"])
        plt.plot(self.historyCallback.history["val_loss"])
        plt.gca().legend(('loss', 'val_loss'))
        plt.savefig(f"{savePath}/4_resultTraining_{self.OpenAirSensorID}.png")

        plt.figure(figsize=(20, 10))
        plt.plot(self.testTarget)
        plt.plot(self.predictionTarget)
        plt.gca().legend(('testTarget', 'predictionTarget'))
        plt.savefig(f"{savePath}/5_prediction_{self.OpenAirSensorID}.png")


def main():
    print("This is a class!")


if __name__ == '__main__':
    main()
