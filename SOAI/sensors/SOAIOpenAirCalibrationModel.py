import tensorflow as tf
import pickle
from pathlib import Path

import logging
logger = logging.getLogger()


## Class which represents a calibration model for the OpenAirSensors
class SOAIOpenAirCalibrationModel():

    ## Initilizes a calibration model which is saved on disk
    #
    # @param pathToModel Path to the model on disk
    def __init__(self, pathToModel, pathToScaler):
        logger.debug(f"Load model from path {pathToModel}")
        self.model = tf.keras.models.load_model(pathToModel)
        self.scaler = pickle.load(open(Path(pathToScaler), 'rb'))  # Path is needed to deal with Windows and Linux

    ## Predicts the value given some features
    #
    # @param data Numpy array with shape (None, 3). The OpenAir sensor is responsible for sorting the features in the correct way.
    # @returns value Predicted value based on the calibration model
    def fPredict(self, data):
        dataNormalized = self.fNormalize(data)
        value = self.model.predict(dataNormalized)
        return value

    ## Since the calibration model was trained with MinMax-normalization it also has to be applied for the prediction
    #
    # @param data Numpy array with shape (None, None)
    # @returns Normalized data
    def fNormalize(self, data):
        dataNormalized = self.scaler.transform(data)
        return dataNormalized
